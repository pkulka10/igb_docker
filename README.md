# Dockerfile to build Docker image for building IGB installers via bitbucket pipeline

# About

To build multi-platform IGB installers for users, we use Install4J from [EJ Technologies](https://www.ej-technologies.com).

The installers contain IGB itself together with a Java run-time environment (JRE). We include a JRE so that IGB users do not have to separately maintain Java on their system. Also, we want to make sure that users get the version of Java we are sure will work well with IGB. 

# Links 

* Install4J 7.0 - https://download-keycdn.ej-technologies.com/install4j/install4j_linux_7_0_8.deb
* JRE Bundles - https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads
* Docker - https://hub.docker.com/r/lorainelab/igb-maven-install4j
      
**Note**: 

Prior to JRE 1.8.0_212, we downloaded JRE bundles used by Install4J from [EJ Technologies bundle download site](https://download.ej-technologies.com/bundles/list) and
got Linux bundles from Oracle. When we upgraded to JRE 1.8.0_212, we could not find new JRE bundles at the download site above, so we made them using the desktop application. 

# How to update

In case of Java version upgrade:

* Add new version files to the JRE bundles download location above and update links in the Dockerfile. 
* Update the Install4J configuration file in IGB project. See distribution folder in the project repository. 
* Update Dockerfile and rebuild the image. Increment image tag.

In case of Install4J upgrade:

* Update Install4J configuration file in the IGB code base. See distribution folder in the project repository. 
* Update Install4J mvn plugin and corresponding profile in IGB POM file. 
* Update the Bitbucket pipelines YML file.
* Update Dockerfile and rebuild the image. Increment image tag.

Review the commit history and associated Jira tickets for insights on how to do above.
